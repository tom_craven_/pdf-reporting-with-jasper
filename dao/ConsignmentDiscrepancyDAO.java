package com.helveta.corvus.energy.report.dao;

import com.helveta.corvus.energy.report.model.ConsignmentDiscrepancyReport;
import com.helveta.corvus.energy.report.model.EnergyConsignmentDiscrepancy;
import com.helveta.platform.model.ProductGrouping;
import com.helveta.platform.model.dao.GenericDAO;
import org.springframework.stereotype.Component;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Tom Craven on 08/10/15.
 * NRG-758: Modification to consignment discrepancy report
 */

@Component("consignmentDiscrepancyDAO")
public class ConsignmentDiscrepancyDAO extends GenericDAO<ProductGrouping, Long> {

    public List<EnergyConsignmentDiscrepancy> getAllConsignments(ConsignmentDiscrepancyReport consignmentDiscrepancyReport) {

        List<EnergyConsignmentDiscrepancy> result = new ArrayList<EnergyConsignmentDiscrepancy>();

        Query query = entityManager.createNativeQuery(("SELECT\n" +
                "  FirstSet.productId,\n" +
                "  ThirdSet.consignmentId,\n" +
                "  FirstSet.originSiteId,\n" +
                "  ThirdSet.destinationSiteId,\n" +
                "  ThirdSet.transporter,\n" +
                "  ThirdSet.transporterReference,\n" +
                "  FirstSet.dateFrom,\n" +
                "  ThirdSet.dateTo,\n" +
                "  FirstSet.productType,\n" +
                "  FirstSet.splitAmount,\n" +
                "  FirstSet.productTag\n" +
                "FROM\n" +
                "  (SELECT\n" +
                "     p.Tag           AS productTag,\n" +
                "     p.DTYPE         AS productType,\n" +
                "     sce.SITE_id     AS originSiteId,\n" +
                "     EVENTDATE       AS dateFrom,\n" +
                "     isDummy,\n" +
                "     psce.PRODUCT_ID AS productId,\n" +
                "     Amount          AS splitAmount\n" +
                "   FROM\n" +
                "     supplychainevent AS sce\n" +
                "     JOIN split AS s ON s.id = sce.id\n" +
                "     JOIN product_supplychainevent AS psce ON psce.SUPPLYCHAINEVENT_ID = sce.id\n" +
                "     JOIN product AS p ON p.id = psce.PRODUCT_ID) AS FirstSet\n" +
                "  INNER JOIN\n" +
                "  (SELECT\n" +
                "     psce.PRODUCT_ID          AS productId,\n" +
                "     SUPPLYCHAINEVENTGROUP_id AS SCEG\n" +
                "   FROM\n" +
                "     energy.combination AS combination\n" +
                "     JOIN supplychainevent AS sce ON combination.id = sce.id\n" +
                "     JOIN product_supplychainevent AS psce ON psce.SUPPLYCHAINEVENT_ID = sce.id) AS SecondSet\n" +
                "    ON FirstSet.productId = SecondSet.productId\n" +
                "  INNER JOIN\n" +
                "  (SELECT\n" +
                "     EVENTDATE                AS dateTo,\n" +
                "     SUPPLYCHAINEVENTGROUP_id AS SCEG,\n" +
                "     pg.TAG                   AS consignmentId,\n" +
                "     Product_ID               AS productId,\n" +
                "     sce.SITE_id              AS originSiteId,\n" +
                "     DESTINATIONSITE_id       AS destinationSiteId,\n" +
                "     TRANSPORTER              AS transporter,\n" +
                "     TRANSPORTERID            AS transporterReference\n" +
                "   FROM\n" +
                "     energy.departure AS departure\n" +
                "     JOIN supplychainevent AS sce ON departure.id = sce.id\n" +
                "     JOIN product_supplychainevent AS psce ON psce.SUPPLYCHAINEVENT_ID = sce.id\n" +
                "     JOIN product AS pg ON pg.id = psce.PRODUCT_ID) AS ThirdSet ON SecondSet.SCEG = ThirdSet.SCEG\n" +
                "WHERE\n" +
                "  isDummy = '0'\n" +
                "  AND (FirstSet.originSiteId=?)\n" +
                "  AND (ThirdSet.destinationSiteId=? OR 1=1)\n" +
                "  AND (FirstSet.dateFrom>=?)\n" +
                "  AND (ThirdSet.dateTo <=?);"));

        query.setParameter(1, consignmentDiscrepancyReport.getOriginSiteId());
        query.setParameter(3, consignmentDiscrepancyReport.getDateFrom());
        query.setParameter(4, consignmentDiscrepancyReport.getDateTo());

        // if site specifies destination otherwise find all
        if (consignmentDiscrepancyReport.getDestinationSiteId() == null) {
            query.setParameter(2, "*");
        } else {
            query.setParameter(2, consignmentDiscrepancyReport.getDestinationSiteId());
        }

        //validate form covers this?
        // query.setParameter("toDate", FormatDateUtils.addDays(consignmentDiscrepancyReport.getDateTo(), 1))

        @SuppressWarnings("unchecked")
        List<Object[]> resultList = query.getResultList();
        for (Object[] objs : resultList) {
            EnergyConsignmentDiscrepancy cd = new EnergyConsignmentDiscrepancy();
            //from result set 1
            cd.setConsignmentId((String) objs[1]);
            cd.setOriginSiteId((Long) objs[2]);
            cd.setDestinationSiteId((Long) objs[3]);
            cd.setTransporter((String) objs[4]);
            cd.setTransportReference((String) objs[5]);
            cd.setDespatchDate((Date) objs[6]);
            cd.setReceiptDate((Date) objs[7]);
            cd.setProductType((String) objs[8]);
            cd.setDespatchAmount((Double) objs[9]);
            cd.setProductID((String) objs[10]);
            // from result set 2
            System.err.print(cd.toString());
            result.add(cd);
        }
        return result;
    }
}


