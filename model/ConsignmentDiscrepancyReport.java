package com.helveta.corvus.energy.report.model;

import com.helveta.platform.components.form.annot.NotNullAndNotEmpty;
import com.helveta.platform.report.ConsignmentDiscrepancyReportPOJOI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.*;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * Created by Tom Craven on 08/10/15.
 * NRG-758: Modification to consignment discrepancy report
 */
@Component("consignmentDiscrepancyReport")
public class ConsignmentDiscrepancyReport implements ConsignmentDiscrepancyReportPOJOI {

    /*
  Search Bean Parameters:
     bound to form following ConsignmentDiscrepancyReport.jspx submit()
       */

    private final DecimalFormat df = new DecimalFormat("0.0##");
    @NotNullAndNotEmpty(message = "Select a site")
    private Long originSiteId;
    private Long destinationSiteId;
    @Past(message = "#FORM-BIND-ERROR Date must be in the past")
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull(message = "#FORM-BIND-ERROR Select the date range")
    private Date dateFrom;
    @Past(message = "#FORM-BIND-ERROR Date must be in the past")
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull(message = "#FORM-BIND-ERROR Select the date range")
    private Date dateTo;

    /*
  Additional report Bean parameters
       */
    @DecimalMax(value = "99.9", message = "#FORM-BIND-ERROR tolerance max")
    @DecimalMin(value = "0.000", message = "#FORM-BIND-ERROR tolerance min")
    private Double tolerance;
    private Locale locale;
    private String productType, unitOfMeasurement, DatePattern, numberPattern;

    @Autowired
    private List<EnergyConsignmentDiscrepancy> consignmentDiscrepancies;

    /*
   Bean Construction
        */

    public ConsignmentDiscrepancyReport() {
    }

    private ConsignmentDiscrepancyReport(Long originSiteId, Long
            destinationSiteId, Date dateFrom, Date dateTo, Double tolerance) {
        this.originSiteId = originSiteId;
        this.destinationSiteId = destinationSiteId;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.tolerance = tolerance;
    }

    public static ConsignmentDiscrepancyReport
    getInstance(Long originSiteId, Long destinationSiteId, Date dateFrom,
                Date dateTo, Double tolerance) {
        return new ConsignmentDiscrepancyReport(originSiteId, destinationSiteId, dateFrom, dateTo, tolerance);
    }

    public void init() {
        this.originSiteId = (long) 0;
        this.destinationSiteId = (long) 0;
        this.tolerance = 0.0;

        Calendar cal = Calendar.getInstance();
        this.dateTo = cal.getTime();
        cal.add(Calendar.DATE, -7);
        this.dateFrom = cal.getTime();
    }

    @SuppressWarnings("EmptyMethod")
    public void destroy() {
        originSiteId = null;
        destinationSiteId = null;
        dateFrom = null;
        dateTo = null;
        tolerance = null;
    }


    /*
 Getters and setters
      */

    @SuppressWarnings({"rawtypes", "unchecked"})
    public List getConsignmentDiscrepancies() {
        return consignmentDiscrepancies;
    }

    @SuppressWarnings("unchecked")
    public void setConsignmentDiscrepancies(@SuppressWarnings("rawtypes") List consignmentDiscrepancies) {
        this.consignmentDiscrepancies = consignmentDiscrepancies;
    }

    public Long getOriginSiteId() {
        return originSiteId;
    }

    @Autowired
    public void setOriginSiteId(Long originSiteId) {
        this.originSiteId = originSiteId;
    }

    public Long getDestinationSiteId() {
        return destinationSiteId;
    }

    @Autowired
    public void setDestinationSiteId(Long destinationSiteId) {
        this.destinationSiteId = destinationSiteId;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    @Autowired
    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    @Autowired
    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Double getTolerance() {
        return tolerance;
    }

    public void setTolerance(Double tolerance) {
        this.tolerance = tolerance;
    }

    @AssertTrue
    public boolean isValidDate() {
        if (!this.dateFrom.before(this.dateTo)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dateFrom);
            cal.add(Calendar.DATE, -7);
            this.dateFrom = cal.getTime();
        }
        return true;
    }

    public String getDoubleForiReport(Double d) {
        return df.format(d);
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getUnitOfMeasurement() {
        return unitOfMeasurement;
    }

    public void setUnitOfMeasurement(String unitOfMeasurement) {
        this.unitOfMeasurement = unitOfMeasurement;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getDatePattern() {
        return DatePattern;
    }

    public void setDatePattern(String datePattern) {
        DatePattern = datePattern;
    }

    public String getNumberPattern() {
        return numberPattern;
    }

    public void setNumberPattern(String numberPattern) {
        this.numberPattern = numberPattern;
    }

    @Override
    public String toString() {
        return "ConsignmentDiscrepancyReport{" +
                "destinationSiteId=" + destinationSiteId +
                ", dateFrom=" + dateFrom +
                ", originSiteId=" + originSiteId +
                ", dateTo=" + dateTo +
                ", tolerance=" + tolerance +
                '}';
    }
}





