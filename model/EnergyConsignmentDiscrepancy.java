package com.helveta.corvus.energy.report.model;

import com.helveta.platform.report.ConsignmentDiscrepancyI;
import com.helveta.platform.report.pojo.ConsignmentDiscrepancy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by Tom on 05/09/2015.
 * NRG-758 Modification to consignment discrepancy Report
 */
@Component("energyConsignmentDiscrepancy")
public class EnergyConsignmentDiscrepancy extends ConsignmentDiscrepancy
        implements ConsignmentDiscrepancyI {

    private Long originSiteId, destinationSiteId;
    private String originName, destinationName, consignmentId, productType;

    public EnergyConsignmentDiscrepancy() {
    }

    public EnergyConsignmentDiscrepancy(String productID, String transporter, String transportReference, Date despatchDate, Date receiptDate, Double despatchAmount, Double receiptAmount, Long originSiteId, Long destinationSiteId, String productType, String consignmentId) {
        super(productID, transporter, transportReference, despatchDate, receiptDate, despatchAmount, receiptAmount);
        this.originSiteId = originSiteId;
        this.destinationSiteId = destinationSiteId;
        this.productType = productType;
        this.consignmentId = consignmentId;
    }

    public static EnergyConsignmentDiscrepancy getInstance(String productID, String transporter, String transportReference, Date despatchDate, Date receiptDate, Double despatchAmount, Double receiptAmount, Long originSiteId, Long destinationSiteId, String productType, String consignmentId) {
        return new EnergyConsignmentDiscrepancy(productID, transporter, transportReference, despatchDate, receiptDate, despatchAmount, receiptAmount, originSiteId, destinationSiteId, productType, consignmentId);
    }

    public Long getOriginSiteId() {
        return originSiteId;
    }

    public void setOriginSiteId(Long originSiteId) {
        this.originSiteId = originSiteId;
    }

    public Long getDestinationSiteId() {
        return destinationSiteId;
    }

    public void setDestinationSiteId(Long destinationSiteId) {
        this.destinationSiteId = destinationSiteId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public String getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(String consignmentId) {
        this.consignmentId = consignmentId;
    }

    @Override
    public String toString() {
        return "EnergyConsignmentDiscrepancy{" +
                "originSiteId=" + originSiteId +
                ", destinationSiteId=" + destinationSiteId +
                ", productType=" + productType +
                ", originName='" + originName + '\'' +
                ", destinationName='" + destinationName + '\'' +
                ", consignmentId='" + consignmentId + '\'' + super.toString() +
                '}';
    }
}
