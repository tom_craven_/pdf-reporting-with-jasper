package com.helveta.corvus.energy.report.business;

import com.helveta.corvus.energy.report.model.ConsignmentDiscrepancyReport;
import com.helveta.corvus.energy.report.model.EnergyConsignmentDiscrepancy;
import com.helveta.platform.business.JasperReportBusiness;
import com.helveta.platform.model.dao.SiteDAO;
import com.helveta.platform.util.ApplicationConfigLoader;
import com.helveta.platform.util.FormatDateUtils;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by Tom on 05/09/2015.
 * NRG-758 Modification to consignment discrepancy Report
 */

@Service("consignmentDiscrepancyReportService")
public class ConsignmentDiscrepancyReportService extends JasperReportBusiness {

    private static final String REPORT_DIR = "/WEB-INF/classes/reports/";
    private static final String[] productTypes = {"testB20", "testB30", "testFossilOil"};
    /*
    Form Validation methods used at @RequestMapping
     */
    private static final Double TOLERANCE_MIN = 0D;
    private static final Double TOLERANCE_MAX = 100D;
    /*
   For use with iReport designer and application test classes
    */
    private static Vector collection;
    private static List<ConsignmentDiscrepancyReport> consignmentDiscrepancyReportTest = new ArrayList<ConsignmentDiscrepancyReport>();
    private static ConsignmentDiscrepancyReport consignmentDiscrepancySearchTest;
    @Autowired
    private ConsignmentDiscrepancySearchService consignmentDiscrepancySearchService;
    @Autowired
    private SiteDAO siteDAO;
    @Autowired
    private MessageSource messageSource;
    private Log logger = LogFactory.getLog(getClass());

    public static List<ConsignmentDiscrepancyReport> generateTestReport() {
        consignmentDiscrepancyReportTest = new ArrayList<ConsignmentDiscrepancyReport>();
        for (String p : productTypes) {
            Locale locale = new Locale.Builder().setLanguage("en").setRegion("GB").build();
            consignmentDiscrepancySearchTest = new ConsignmentDiscrepancyReport();
            Date date = new Date();
            consignmentDiscrepancySearchTest.setNumberPattern("0.0##");
            consignmentDiscrepancySearchTest.setTolerance(1.0);
            consignmentDiscrepancySearchTest.setDateFrom(date);
            consignmentDiscrepancySearchTest.setDateTo(date);
            consignmentDiscrepancySearchTest.setLocale(locale);
            consignmentDiscrepancySearchTest.setOriginSiteId((long) 654);
            consignmentDiscrepancySearchTest.setDestinationSiteId((long) 652);
            consignmentDiscrepancySearchTest.setDatePattern(FormatDateUtils.getLocalePattern());
            consignmentDiscrepancySearchTest.setConsignmentDiscrepancies(generateTestCollection(date, date, p));
            consignmentDiscrepancySearchTest.setUnitOfMeasurement("TestMeasures");
            consignmentDiscrepancySearchTest.setProductType(p);
            consignmentDiscrepancySearchTest.setLocale(locale);
            consignmentDiscrepancyReportTest.add(consignmentDiscrepancySearchTest);
        }
        return consignmentDiscrepancyReportTest;
    }

    //test collection for iReport
    public static List generateTestCollection(String p) {
        collection = new Vector();
        for (Integer i = 1; i < 21; i++) {
            long originSiteIdT = ((long) 654);
            long destinationSiteIdT = ((long) 652);
            String productIDT = p + "#" + i;
            String transporterT = "Transporter: " + i;
            String transportReferenceT = "Vehicle# " + (i * i) + "" + i;
            Double despatchAmountT = 100.0 + (i * i);
            Double receiptAmountT = 50.0 + (i * i);
            String consignmentIdT = p + " consignmentent" + i;
            Date date = new Date();
            EnergyConsignmentDiscrepancy e = EnergyConsignmentDiscrepancy.getInstance(productIDT, transporterT, transportReferenceT,
                    date, date, despatchAmountT, receiptAmountT, originSiteIdT, destinationSiteIdT, productIDT, consignmentIdT);
            e.setDestinationName("TestDestination " + i);
            e.setOriginName("TestOrigin " + i);
            //noinspection unchecked
            collection.add(e);
        }
        return collection;
    }

    //test collection for main app
    private static List generateTestCollection(Date dateFrom, Date dateTo, String p) {
        collection = new Vector();
        for (Integer i = 1; i < 21; i++) {
            long originSiteIdT = ((long) 654);
            long destinationSiteIdT = ((long) 652);
            String productIDT = p + "#" + i;
            String transporterT = "Transporter: " + i;
            String transportReferenceT = "Vehicle# " + (i * i) + "" + i;
            Double despatchAmountT = 100.0 + (i * i);
            Double receiptAmountT = 50.0 + (i * i);
            String consignmentIdT = p + " consignmentent" + i;
            EnergyConsignmentDiscrepancy e = EnergyConsignmentDiscrepancy.getInstance(productIDT, transporterT, transportReferenceT,
                    dateFrom, dateTo, despatchAmountT, receiptAmountT, originSiteIdT, destinationSiteIdT, productIDT, consignmentIdT);
            e.setDestinationName("TestDestination " + i);
            e.setOriginName("TestOrigin " + i);
            //noinspection unchecked
            collection.add(e);
        }
        return collection;
    }

    public JRDataSource loadConsignmentDiscrepancyReportData(MessageSource messageSource,
                                                             ConsignmentDiscrepancyReport consignmentDiscrepancyReport) {
         /*
         use test values for scaleability test or iReport designer:
         List<ConsignmentDiscrepancyReport> discrepancyReports = generateTestReport();
          */
        List<ConsignmentDiscrepancyReport> discrepancyReports = consignmentDiscrepancySearchService.findConsignmentDiscrepancies(consignmentDiscrepancyReport, messageSource);

        return new JRBeanCollectionDataSource(discrepancyReports);
    }

    public ConsignmentDiscrepancyReport putReportParams(HttpServletRequest request, MessageSource messageSource,
                                                        ConsignmentDiscrepancyReport consignmentDiscrepancyReport,
                                                        Map<String, Object> reportParams) {
        /*
        Report: energyConsignmentDiscrepancyReport.jrxml
        Configuration Parameters
        */

        Locale locale = RequestContextUtils.getLocale(request);
        consignmentDiscrepancyReport.setNumberPattern(ApplicationConfigLoader.getProperties().getProperty(
                "consignment.discrepancy.report.amount.format"));
        consignmentDiscrepancyReport.setDatePattern(FormatDateUtils.getLocalePattern());
        //record local for iReport Preview
        consignmentDiscrepancyReport.setLocale(locale);
        /*
Labels: Main Report
 */

        reportParams.put("consignmentDiscrepancyReportTitle", messageSource.getMessage(
                "consignment_discrepancy_report_title_label", null, locale));
        reportParams.put("noDataFoundLabel", messageSource.getMessage("label_no_data_found", null, locale));
        reportParams.put("dateRangeLabel", messageSource.getMessage("consignment_discrepancy_report_date_range_label",
                null, locale));
        reportParams.put("toleranceLabel", messageSource.getMessage("consignment_discrepancy_report_tolerance_label",
                null, locale));
        reportParams.put("productTypeLabel", messageSource.getMessage(
                "consignment_discrepancy_report_product_type_label", null, locale));
        reportParams.put("toLabel", messageSource.getMessage(
                "consignment_discrepancy_report_to_label", null, locale));
        reportParams.put("noDataFoundLabel", messageSource.getMessage("label_no_data_found", null, locale));
        /*
Labels: Result Set Table
 */
        reportParams.put("productIdLabel", messageSource.getMessage("consignment_discrepancy_report_product_id_label",
                null, locale));
        reportParams.put("consignmentIdLabel", messageSource.getMessage("consignment_discrepancy_report_consignment_id_label",
                null, locale));
        reportParams.put("originLabel", messageSource.getMessage(
                "consignment_discrepancy_report_site_label", null, locale));
        reportParams.put("destinationLabel", messageSource.getMessage(
                "consignment_discrepancy_report_destination_label", null, locale));
        reportParams.put("transporterLabel", messageSource.getMessage(
                "consignment_discrepancy_report_transporter_label", null, locale));
        reportParams.put("transportRefLabel", messageSource.getMessage(
                "consignment_discrepancy_report_transport_reference_label", null, locale));
        reportParams.put("despatchDateLabel", messageSource.getMessage(
                "consignment_discrepancy_report_despatch_date_label", null, locale));
        /*
Labels: Mass balance
 */
        reportParams.put("amountLabel", messageSource.getMessage("consignment_discrepancy_report_amount_label", null,
                locale));
        reportParams.put("amountOnDespatchLabel", messageSource.getMessage(
                "consignment_discrepancy_report_amount_on_despatch_label", null, locale));
        reportParams.put("amountOnReceiptLabel", messageSource.getMessage(
                "consignment_discrepancy_report_amount_on_receipt_label", null, locale));
        reportParams.put("amountDifferenceLabel", messageSource.getMessage(
                "consignment_discrepancy_report_amount_difference_label", null, locale));

        return consignmentDiscrepancyReport;
    }

    public boolean isDateFromBeforeDateTo(String strDateFrom, String strDateTo) {
        Date dateFrom = FormatDateUtils.parseDate(strDateFrom);
        Date dateTo = FormatDateUtils.parseDate(strDateTo);
        return dateFrom.compareTo(dateTo) <= 0;
    }

    public boolean checkTolerance(Double tolerance) {
        return TOLERANCE_MIN.doubleValue() <= tolerance.doubleValue() && tolerance.doubleValue() <= TOLERANCE_MAX.doubleValue();
    }

    public void setConsignmentDiscrepancySearchService(ConsignmentDiscrepancySearchService consignmentDiscrepancySearchService) {
        this.consignmentDiscrepancySearchService = consignmentDiscrepancySearchService;
    }
}


