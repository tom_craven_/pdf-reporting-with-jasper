package com.helveta.corvus.energy.report.business;

import com.helveta.corvus.energy.report.dao.ConsignmentDiscrepancyDAO;
import com.helveta.corvus.energy.report.model.ConsignmentDiscrepancyReport;
import com.helveta.corvus.energy.report.model.EnergyConsignmentDiscrepancy;
import com.helveta.platform.business.SiteBusiness;
import com.helveta.platform.config.UoMConfiguration;
import com.helveta.platform.model.*;
import com.helveta.platform.model.dao.ModificationDAO;
import com.helveta.platform.model.dao.ProductGroupingDAO;
import com.helveta.platform.model.dao.SiteDAO;
import com.helveta.platform.product.ProductConfiguration;
import com.helveta.platform.util.SessionAndDBUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by Tom on 05/09/2015.
 * NRG-758 Modification to consignment discrepancy Report
 */

@Service("consignmentDiscrepancySearchService")
public class ConsignmentDiscrepancySearchService {

    protected static final Double TOLERANCE_MIN = 0D;

    protected static final Double TOLERANCE_MAX = 100D;
    @Autowired
    private ProductGroupingDAO productGroupingDAO;
    @Autowired
    private ProductConfiguration productConfiguration;
    @Autowired
    private
    ConsignmentDiscrepancyDAO consignmentDiscrepancyDAO;
    @Autowired
    private SiteBusiness siteBusiness;
    @Autowired
    private SessionAndDBUtil sessionAndDBUtil;
    @Autowired
    private SiteDAO siteDAO;
    @Autowired
    private ModificationDAO modificationDAO;
    @Autowired
    private UoMConfiguration uoMConfiguration;

    private List<EnergyConsignmentDiscrepancy> loadReceivedConsignments(ConsignmentDiscrepancyReport consignmentDiscrepancySearch) {
        return consignmentDiscrepancyDAO.getAllConsignments(consignmentDiscrepancySearch);
    }

    List<ConsignmentDiscrepancyReport> findConsignmentDiscrepancies(ConsignmentDiscrepancyReport consignmentDiscrepancySearch, MessageSource
            messageSource) {

        Set<String> productTypes = productConfiguration.getProductTypeController().getProductTypes().keySet();

        Map<String, List<EnergyConsignmentDiscrepancy>> consignmentDiscrepancyMap = new HashMap<String, List<EnergyConsignmentDiscrepancy>>();
        for (String productType : productTypes) {
            consignmentDiscrepancyMap.put(productType, new ArrayList<EnergyConsignmentDiscrepancy>());
        }
        List<EnergyConsignmentDiscrepancy> receivedConsignments = loadReceivedConsignments(consignmentDiscrepancySearch);

        // If consignment contains groupings get id of nested product
        for (EnergyConsignmentDiscrepancy receivedConsignment : receivedConsignments) {
            populateSiteName(receivedConsignment);
            List<ProductGrouping> productGroupings = productGroupingDAO
                    .findProductGroupingByTagEquals(receivedConsignment.getConsignmentId());
            if (!productGroupings.isEmpty()) {
                ProductGrouping productGrouping = productGroupings.get(0);
                // get the base product's details
                for (Product product : productGrouping.getConsignedProducts()) {
                    receivedConsignment.setProductID(product.getTag());
                    receivedConsignment.setReceiptAmount(product.getAmount());
                    modificationDAO.getModifications(product);
                    //list modifications to products in consignment
                    List<Modification> amountModificationsAfterSeparation = modificationDAO.getModifications(product);
                    // enter amount data detecting discrepancies
                    if (amountModificationsAfterSeparation.isEmpty()) {
                        receivedConsignment.setDespatchAmount(product.getAmount());
                        receivedConsignment.setReceiptAmount(product.getAmount());
                    } else {
                        receivedConsignment.setDespatchAmount(Double
                                .parseDouble(amountModificationsAfterSeparation.get(0).getPreviousValue()));
                        receivedConsignment.setReceiptAmount(Double.parseDouble(amountModificationsAfterSeparation
                                .get(amountModificationsAfterSeparation.size() - 1).getNewValue()));
                    }
                    double percentDifference = receivedConsignment.getPercentDifference();

                    if (checkInToleranceLimit(consignmentDiscrepancySearch.getTolerance(), percentDifference)
                            && consignmentDiscrepancyMap.containsKey(product.getClass().getName())) {
                        consignmentDiscrepancyMap.get(product.getClass().getName()).add(receivedConsignment);
                    }
                }
            }
        }
        List<ConsignmentDiscrepancyReport> consignmentDiscrepancyReports = new ArrayList<ConsignmentDiscrepancyReport>();

        for (Map.Entry<String, List<EnergyConsignmentDiscrepancy>> entry : consignmentDiscrepancyMap.entrySet()) {
            if (!entry.getValue().isEmpty()) {
                String uomI18nKey = uoMConfiguration.getClassUOMMapping().get(entry.getKey()).getI18nKey();
                String uom = messageSource.getMessage(uomI18nKey, null, LocaleContextHolder.getLocale());
                // each Report has base values copied from search sorted by product type
                ConsignmentDiscrepancyReport consignmentDiscrepancyReport = consignmentDiscrepancySearch;

                consignmentDiscrepancyReport.setProductType(productConfiguration.getProductTypeController()
                        .getProductTypes().get(entry.getKey()).toString());
                consignmentDiscrepancyReport.setConsignmentDiscrepancies(entry.getValue());
                consignmentDiscrepancyReport.setUnitOfMeasurement(uom.replace("<sup>", "").replace("</sup>", ""));
                consignmentDiscrepancyReports.add(consignmentDiscrepancyReport);
            }
        }
        return consignmentDiscrepancyReports;
    }

    public ModelAndView checkAuthorityPopulateFormComponents(HttpServletRequest request) {
        // get context
        WebApplicationContext applicationContext = RequestContextUtils
                .getWebApplicationContext(request);
        ModelAndView mv = new ModelAndView("consignmentDiscrepancyReports/consignmentDiscrepancyReport");
        Map<String, Object> model = mv.getModel();
        //get form backing bean
        ConsignmentDiscrepancyReport consignmentDiscrepancyReport = (ConsignmentDiscrepancyReport) applicationContext.getBean("consignmentDiscrepancyReport");
        consignmentDiscrepancyReport.init();
        if (consignmentDiscrepancyReport.getDateFrom().compareTo(consignmentDiscrepancyReport.getDateTo()) == 0) {
            long DAY_IN_MS = 1000 * 60 * 60 * 24;
            Date dateFromDefault = new Date(System.currentTimeMillis() - (7 * DAY_IN_MS));
            consignmentDiscrepancyReport.setDateFrom(dateFromDefault);
            consignmentDiscrepancyReport.setDateTo(new Date());
        }
        model.put("consignmentDiscrepancyReport", consignmentDiscrepancyReport);
        //authorise site picker
        sessionAndDBUtil = applicationContext.getBean(SessionAndDBUtil.class);
        UserProfile currentUser = sessionAndDBUtil
                .getCurrentUserProfile(request);
        siteBusiness = applicationContext
                .getBean(SiteBusiness.class);
        request.setAttribute("callBy", "SITES");
        boolean authorised = siteBusiness.isManageAllSites(
                currentUser, "SITES");
        model.put("isAdminApplication", authorised);

        if (authorised) { /// if you are an admin
            model.put(("destinationSites"), siteBusiness.parseJsonForMultiLevelSearch5160(request));
            model.put("sites", siteBusiness.parseJsonForMultiLevelSearch5160(request));

            return mv;
        } else {  /// if you are not an admin
            model.put("destinationSitesC", siteBusiness.parseJsonForMultiLevelSearchSite(request));
            model.put("sitesC", siteBusiness.parseJsonForMultiLevelSearchSite(request));

            return mv;
        }

    }

    private void populateSiteName(EnergyConsignmentDiscrepancy energyConsignmentDiscrepancy) {
        Site origin = siteDAO.findEntity(energyConsignmentDiscrepancy.getOriginSiteId());
        Site destination = siteDAO.findEntity(energyConsignmentDiscrepancy.getDestinationSiteId());
        energyConsignmentDiscrepancy.setDestinationName(destination.getName());
        energyConsignmentDiscrepancy.setOriginName(origin.getName());
    }

    private boolean checkInToleranceLimit(double tolerance, double percentDifference) {
        return percentDifference <= 0 - tolerance || percentDifference >= tolerance;
    }

}