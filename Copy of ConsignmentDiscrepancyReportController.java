package com.helveta.platform.web.report;

import com.helveta.corvus.energy.report.business.ConsignmentDiscrepancyReportService;
import com.helveta.corvus.energy.report.business.ConsignmentDiscrepancySearchService;
import com.helveta.corvus.energy.report.model.ConsignmentDiscrepancyReport;
import com.helveta.platform.business.JasperReportBusiness;
import com.helveta.platform.components.ex.InvalidUserInputException;
import com.helveta.platform.util.FormatNumberUtils;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Consignment Discrepancy Report Controller.
 *
 * @author Minh Le, Tom Craven
 */
@RequestMapping("/consignmentDiscrepancyReports")
@Controller
@SessionAttributes("consignmentDiscrepancyJasperPrint")
@Scope("session")
class ConsignmentDiscrepancyReportController extends ReportController {

    private final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private JasperReportBusiness jasperReportBusiness;
    @Autowired
    private ConsignmentDiscrepancySearchService consignmentDiscrepancySearchService;
    @Autowired
    private ConsignmentDiscrepancyReportService consignmentDiscrepancyReportService;


    @RequestMapping(value = "/create")
    public ModelAndView displayCustomerForm(HttpServletRequest request) {

        return consignmentDiscrepancySearchService.checkAuthorityPopulateFormComponents(request);
    }


    @RequestMapping(value = "/search")
    public void getConsignmentDiscrepancyReport(@Valid ConsignmentDiscrepancyReport consignmentDiscrepancyReport,
                                                HttpServletRequest request, BindingResult result,
                                                Model uiModel, HttpServletResponse response)
            throws IOException, InvalidUserInputException {
        if (result.hasErrors()) {
            System.err.println("Form does not validate");
            List<ObjectError> errors = result.getAllErrors();
            for (ObjectError error : errors) {
                System.err.print("CONSIGNMENT DISCREPANCY " + error.toString());
            }
        }
        String reportName = "consignmentDiscrepancyReport";
        Map<String, Object> mapParams = new HashMap<String, Object>();
        consignmentDiscrepancyReport = consignmentDiscrepancyReportService.putReportParams(request, messageSource, consignmentDiscrepancyReport, mapParams);
        JRDataSource dataSource = consignmentDiscrepancyReportService.loadConsignmentDiscrepancyReportData(
                messageSource, consignmentDiscrepancyReport);
        JasperPrint jasperPrint = jasperReportBusiness.getJasperPrint(request, uiModel, response, reportName,
                mapParams, dataSource);
        uiModel.addAttribute("consignmentDiscrepancyJasperPrint", jasperPrint);

        jasperReportBusiness.showReport(jasperPrint, response.getWriter(), request);
    }

    @RequestMapping(value = "/exportpdf")
    public void exportPDF(@RequestParam(value = "name", required = false) String reportName,
                          @ModelAttribute("consignmentDiscrepancyJasperPrint") JasperPrint consignmentDiscrepancyJasperPrint,
                          SessionStatus status,
                          HttpServletRequest request,
                          Model uiModel, HttpServletResponse response) throws InvalidUserInputException {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=consignmentDiscrepancyReport.pdf");
        try {
            JRPdfExporter jrPdfExporter = this
                    .exportpdf(request, uiModel, response, consignmentDiscrepancyJasperPrint);
            jrPdfExporter.exportReport();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }
        status.setComplete();
    }

    @RequestMapping(value = "/exportexcel")
    public void exportExcel(@RequestParam(value = "name", required = false) String reportName,
                            @ModelAttribute("consignmentDiscrepancyJasperPrint") JasperPrint consignmentDiscrepancyJasperPrint,
                            HttpServletRequest request, SessionStatus status,
                            Model uiModel, HttpServletResponse response) throws InvalidUserInputException {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=consignmentDiscrepancy.xls");
        try {
            JExcelApiExporter jrXlsExporter = this.exportexcel(request, uiModel, response,
                    consignmentDiscrepancyJasperPrint);
            jrXlsExporter.exportReport();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }
        status.setComplete();
    }


    @RequestMapping(value = "/checkDateFromBeforeDateTo", method = RequestMethod.POST)
    @ResponseBody
    public boolean checkDateFromBeforeDateTo(HttpServletRequest request) {
        String dateFrom = request.getParameter("dateFrom");
        String dateTo = request.getParameter("dateTo");
        return consignmentDiscrepancyReportService.isDateFromBeforeDateTo(dateFrom, dateTo);
    }


    @RequestMapping(value = "/checkTolerance", method = RequestMethod.POST)
    @ResponseBody
    public boolean checkTolerance(HttpServletRequest request) {
        String strTolerance = request.getParameter("tolerance");
        try {
            Double tolerance = FormatNumberUtils.getDoubleValueBaseOnLocale(strTolerance, null);
            return consignmentDiscrepancyReportService.checkTolerance(tolerance);
        } catch (InvalidUserInputException e) {
            // The tolerance is not a number, we must return false to client to
            // show error message.
            logger.error("The tolerance is not a number", e);
            return false;
        }
    }

    public void setConsignmentDiscrepancyReportService(ConsignmentDiscrepancyReportService consignmentDiscrepancyReportService) {
        this.consignmentDiscrepancyReportService = consignmentDiscrepancyReportService;
    }

    public void setConsignmentDiscrepancySearchService(ConsignmentDiscrepancySearchService consignmentDiscrepancySearchService) {
        this.consignmentDiscrepancySearchService = consignmentDiscrepancySearchService;
    }
}

